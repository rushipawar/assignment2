import socket
import select
import time
import sys
import redis
from cb import circuitbreaker
from itertools import islice, cycle
from flask import jsonify

arr = [1,2,3]
redis_arr=[]
servers_stopped= []
redis_arr.append(1)


server_1 = circuitbreaker()
server_2 = circuitbreaker()
server_3 = circuitbreaker()
server_1.state_close_it()
server_2.state_close_it()
server_3.state_close_it()


def it_works(port):
    if (port in servers_stopped):
        if (port == 5010):
            server_1.statechecker()
            if (server_2.getState() == 2):
                position = get_pos(5010)

                del servers_stopped[position]
                new_port = port

            else:
                print "*****************", server_1.getState()
                new_port = getPort()

        if(port == 5020):

            server_2.statechecker()
            if (server_2.getState() == 2):
                position = get_pos(5020)
                del servers_stopped[position]
                new_port=port

            else:
                print "*****************",server_2.getState()
                new_port = getPort()


        if(port == 5030):
            server_3.statechecker()
            if (server_3.getState() == 2):
                position = get_pos(5030)
                del servers_stopped[position]
                new_port = port

            else:
                print "*****************", server_3.getState()
                new_port = getPort()


    else:
        new_port = port
    return new_port


def get_pos(port):
    length  = len(servers_stopped)
    if (length == 1):
        return 0
    elif length ==2:
        if(servers_stopped[0]==port):
            return 0
        elif(servers_stopped[1]==port):
            return 1
    elif length == 3:
        if (servers_stopped[0] == port):
            return 0
        elif (servers_stopped[1] == port):
            return 1
        elif (servers_stopped[2] == port):
            return 2






def getPort():
    r = redis.StrictRedis(host='127.0.0.1', port=6379, db=0)
    v = int(r.get(redis_arr[0]))
    if redis_arr[0] == 1:
        del redis_arr[0]
        redis_arr.append(2)
    elif redis_arr[0]==2:
        del redis_arr[0]
        redis_arr.append(3)
    elif redis_arr[0] == 3:
        del redis_arr[0]
        redis_arr.append(1)
    temp = it_works(v)

    return temp





buffer_size = 4096
delay = 0.0001
host = '127.0.0.1'
forward_to = (host,getPort())



class YeLoServer:
    input_ = []
    serv_channel = {}

    def __init__(self, host, port):
        self.d = None
        self.s = None
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((host, port))
        self.server.listen(200)

    def chan_recv(self):
        d = self.d
        print d
        self.serv_channel[self.s].send(d)


    def chan_on_close(self):
        print self.s.getpeername(), "is now disconnected"

        self.input_.remove(self.s)
        self.input_.remove(self.serv_channel[self.s])
        out = self.serv_channel[self.s]
        # close cvlient connection
        self.serv_channel[out].close()  # equivalent to do self.s.close()
        # close remote server conn
        self.serv_channel[self.s].close()
        del self.serv_channel[out]
        del self.serv_channel[self.s]


    def primary_loop(self):
        self.input_.append(self.server)
        while 1:
            time.sleep(delay)
            ss = select.select
            inputready, outputready, exceptready = ss(self.input_, [], [])
            for self.s in inputready:
                if self.s == self.server:
                    print "First Connection"
                    self.chan_on_accept()
                    break
                print "Dooja Connection"
                self.data = self.s.recv(buffer_size)
                if len(self.data) == 0:
                    self.chan_on_close()
                    break
                else:
                    self.chan_recv()

    def chan_on_accept(self):

        tmp=getPort()

        frwd = Fwd().start('', tmp)
        clientsock, clientaddr = self.server.accept()
        if frwd:
            print clientaddr, "is now connected"
            self.input_.append(clientsock)
            self.input_.append(frwd)
            self.serv_channel[clientsock] = frwd
            self.serv_channel[frwd] = clientsock

        else:
            print "Failed to establish remote server connection...",
            print "Closing client side connection...", clientaddr

            if tmp == 5010:
                server_1.handle_failure()
                state1 = server_1.getState()
                if state1 == 0:
                    servers_stopped.append(tmp)
                    print "Unfortunately, the server has stopped running... :(",servers_stopped

            elif tmp == 5020:
                server_2.handle_failure()
                state2 = server_2.getState()
                if (state2 == 0):
                    servers_stopped.append(tmp)
                    print "Unfortunately, the server has stopped running... :(",servers_stopped

            elif tmp==5030:
                server_3.handle_failure()
                state3 = server_3.getState()
                if state3 == 0:
                    servers_stopped.append(tmp)
                    print "Unfortunately, the server has stopped running... :(",servers_stopped




            clientsock.close()




class Fwd:
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start(self, host, port):
        try:
            self.forward.connect((host, port))
            return self.forward
        except Exception, e:
            print e
            return False



if __name__ == '__main__':
        server = YeLoServer('', 9091)
        try:
            server.main_loop()
        except KeyboardInterrupt:
            print "Server Stopping by Ctrl+C"
            sys.exit(1)
