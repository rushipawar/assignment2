from flask import *
from flask_sqlalchemy import *
import sqlalchemy
import socket
import urllib2

from flask import Flask
from flask_sqlalchemy import *
import redis

app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI']= 'mysql://root:root@localhost/sampledb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= True
new_db = SQLAlchemy(app)
DATABASE='sampledb'


class DBTEST(new_db.Model):
    __tablename__ = 'sampleTable'
    id = new_db.Column('id', new_db.Integer, primary_key = 'True')
    name = new_db.Column('name',new_db.String(20))
    email = new_db.Column('email', new_db.String(20))
    category = new_db.Column('category', new_db.String(20))
    description = new_db.Column('description', new_db.String(2000))
    link = new_db.Column('link', new_db.String(300))
    estimated_costs = new_db.Column('estimated_costs', new_db.String(20))
    submit_date = new_db.Column('submit_date', new_db.String(20))
    status = new_db.Column('status', new_db.String(20))
    decision_date = new_db.Column('decision_date', new_db.String(200))


    def __init__(self,name= '', email='',category='',description='',link='',estimated_costs='',submit_date='',status='',decision_date=''):


        self.name= name
        self.email = email
        self.category= category
        self.description= description
        self.link= link
        self.estimated_costs= estimated_costs
        self.submit_date= submit_date
        self.status= status
        self.decision_date=decision_date

class Createdatabase():
    engine = sqlalchemy.create_engine('mysql://root:root@localhost/sampledb')
    engine.execute("CREATE DATABASE IF NOT EXISTS %s; " %(DATABASE))
    engine.execute("USE sampledb ;")
    new_db.create_all()
    new_db.session.commit()

@app.route('/v1/expenses/<int:r>', methods=['GET'])
def index(r):
    x = DBTEST.query.filter_by(id=r).first()
    if x is not None:
        temp_var = {
            "id": x.id,
            "name": x.name,
            "email": x.email,
            "category": x.category,
            "description": x.description,
            "link": x.link,
            "submit_date": x.submit_date,
            "estimated_costs": x.estimated_costs,
            "status": x.status,
            "decision_date": x.decision_date}
        temp_var2 = Response(response=json.dumps(temp_var), status=200)
        return temp_var2
    else:
        temp_var2= Response(status=404)
        return temp_var2


@app.route('/v1/expenses/<int:rid>', methods=['DELETE'])
def deleterequest(rid):
    m= new_db.session.query(DBTEST).filter_by(id=rid).first()
    if m is not None:
        new_db.session.delete(m)
        new_db.session.commit()
        return (Response(status=204))


@app.route('/v1/expenses/<int:r_ID>', methods=['PUT'])
def putmethod(r_ID):
    x = request.get_json(force=True)
    updateRow = DBTEST.query.filter_by(id=r_ID)
    if updateRow != None:
        for key, value in x.items():
            updateRow.update({key: value})
            new_db.session.commit()
            temp_new = Response(status=202)
            return temp_new



@app.route('/v1/expenses', methods=['POST'])
def postrequest():
    x= request.get_json(force=True)

    name = x['name']
    email = x['email']
    category = x['category']
    description = x['description']
    link = x['link']
    estimated_costs = x['estimated_costs']
    submit_date = x['submit_date']
    status = "pending"
    decision_date = ""

    row = DBTEST(name, email, category, description, link, estimated_costs, submit_date, status, decision_date)
    new_db.session.add(row)
    new_db.session.commit()

    temp_var = {'id': row.id,
           'name': row.name,
           'email': row.email,
           'category': row.category,
           'description': row.description,
           'link': row.link,
           'estimated_costs': row.estimated_costs,
           'submit_date': row.submit_date,
           'status': row.status,
           'decision_date': row.decision_date
           }
    temp_var2 = Response(response=json.dumps(temp_var), status=201, mimetype="application/json")
    return temp_var2





if __name__ == "__main__":
    red_serve= redis.StrictRedis(host='127.0.0.1', port= 6379, db=0)
    red_serve.set(3,'5030')
    app.run(debug=True, host='0.0.0.0',port=5030)
