import time

STATE_OPEN = 0
STATE_CLOSED = 1
STATE_HALF_OPEN = 2


class circuitbreaker(object):
    def __init__(self, fails=3, retry_timeout=10, function_val=None, exceptions_allow=None,
                 exceptions_fails=None):

        print 'CircuitBreaker Configs'

        self.fails = fails
        self.retry_timeout = retry_timeout

        self.failure_count = 0
        self.state = STATE_OPEN
        self.hlt = 0

        self.function_val = function_val


        if exceptions_allow is not None:
            self.exceptions_allow = tuple(exceptions_allow)
        else:
            self.exceptions_allow = ()


        if exceptions_fails is not None:
            self.exceptions_fails = tuple(exceptions_fails)
        else:
            self.exceptions_fails = ()

    def state_open_it(self):

        self.state = STATE_OPEN
        self.time_open = time.time()
        self.hlt = self.time_open + self.retry_timeout
        print 'CircuitBreaker State', self.state

    def state_close_it(self):

        self.state = STATE_CLOSED
        self.failure_count = 0
        print 'Circuitbreaker State', self.state

    def state_open_it_half(self):

        self.state = STATE_HALF_OPEN
        print 'Circuitbreaker State', self.state

    def statechecker(self):
        try:
            if self.state == STATE_OPEN:
                now = time.time()
                if now >= self.hlt:
                    self.state_open_it_half()
                    raise circuitbreakexceptions('The Circuit is in Half open state')
            return self.state
        except circuitbreakexceptions, temp:
            print temp.msg

    def handle_failure(self):

        try:
            self.failure_count += 1
            if self.failure_count >= self.fails:
                self.state_open_it()
                raise circuitbreakexceptions('Circuit is Open')
        except circuitbreakexceptions, cx:
            print cx.msg

    def handle_success(self):

        self.failure_count = 0
        self.state_close_it()

    def getState(self):
        return self.state
# circuit breaker exception class
class circuitbreakexceptions(Exception):
    def __init__(self, msg):
        self.msg = msg
